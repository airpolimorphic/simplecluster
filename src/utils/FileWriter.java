package utils;

import java.io.*;

public class FileWriter {

    public <T> void save(T data, String path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(path)))) {
            oos.writeObject(data);
            oos.flush();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    public <T> T load(String path) {
        T out;
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path)))) {
            out = (T) ois.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return out;
    }


    public void writeBytes(byte[] array, String path){
        try (FileOutputStream fos = new FileOutputStream(path)) {
            fos.write(array);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void appendFile(byte[] array, String path) {
        if(new File(path).exists()) {
            try(FileOutputStream saltOutFile = new FileOutputStream(path, true)) {
                saltOutFile.write(array);
            }catch (IOException e){
                e.printStackTrace();
            }
        }else{
            writeBytes(array, path);
        }
    }

}
