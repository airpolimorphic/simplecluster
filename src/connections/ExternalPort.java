package connections;

import exceptions.PortForwardingException;
import org.bitlet.weupnp.GatewayDevice;
import org.bitlet.weupnp.GatewayDiscover;
import org.bitlet.weupnp.PortMappingEntry;
import org.xml.sax.SAXException;
import utils.FileWriter;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

public class ExternalPort {

    private String myExternalIp;
    private String myInternalIp;
    private int externalServerPort;
    private String lastPortPath = "./settings.s";
    private GatewayDevice device;
    private String description;
    private FileWriter fileWriter = new FileWriter();


    public void removePortUpnp() {
        int loadedPort = fileWriter.load(lastPortPath);
        removePortUpnp(loadedPort);
    }

    public void removePortUpnp(int port) {
        try {
            if (device == null) {
                GatewayDiscover discover = new GatewayDiscover();
                discover.discover();
                device = discover.getValidGateway();
            }
            if (device == null) throw new PortForwardingException("Router not found!");
            if (device.isConnected()) {
                System.out.println("Router " + getRouterModel(device) + " connected");
            } else {
                //here may be error
                System.out.println("Router " + getRouterModel(device) + " disconnected");
                throw new PortForwardingException("Port forwarding failure");
            }
            boolean hasPort = device.getSpecificPortMappingEntry(port, "TCP", new PortMappingEntry());
            if (hasPort) {
                boolean res = device.deletePortMapping(port, "TCP");
                String message = res ? "Port has been removed" : "Failure, port has NOT been removed!";
                System.out.println(message);
            }
            new File(lastPortPath).delete();
        } catch (IOException | SAXException | ParserConfigurationException e) {
            throw new PortForwardingException(e);
        }
    }

    public int forwardPortUpnp(int port, String description) {//sets all ext ip, int ip, ext port
        this.description = description;
        try {
            int forwardLimit = 50000;
            GatewayDiscover discover = new GatewayDiscover();
            discover.discover();
            device = discover.getValidGateway();
            if (device == null) throw new PortForwardingException("Router not found!");
            if (device.isConnected()) {
                System.out.println("Router " + getRouterModel(device) + " connected");
            } else {
                //here may be error
                System.out.println("Router " + getRouterModel(device) + " disconnected");
            }
            int externalPort = port;
            InetAddress localAddress = device.getLocalAddress();
            String externalIPAddress = device.getExternalIPAddress();
            myExternalIp = externalIPAddress;
            myInternalIp = localAddress.getHostAddress();


            boolean res1 = hasPreviousPort(externalPort);
            boolean res2 = isPortInRouter(externalPort);
            if (hasPreviousPort(externalPort) && isPortInRouter(externalPort)) {
                System.out.println("Port forwarded previously: " + externalPort);
                return externalPort; //already in local storage
            }
            System.out.println(res1 + " " + res2);
            while (isPortInRouter(externalPort) && forwardLimit > 0 || !forwardPort(externalPort, externalPort, localAddress) && forwardLimit > 0) {
                System.out.println("Searching for free external port, " + externalPort);
                externalPort++;
                forwardLimit--;
            }
            if (forwardLimit <= 0)
                throw new PortForwardingException("Number attempts to forward port reached to a limit");

        } catch (IOException | SAXException | ParserConfigurationException e) {
            throw new PortForwardingException(e);
        }
        return externalServerPort;
    }

    //region GETTERS AND SETTERS
    public String getMyExternalIp() {
        return myExternalIp;
    }

    public String getMyInternalIp() {
        return myInternalIp;
    }

    public int getExternalServerPort() {
        return externalServerPort;
    }

    public void setLastPortPath(String lastPortPath) {
        this.lastPortPath = lastPortPath;
    }

    public String getLastPortPath() {
        return lastPortPath;
    }
    //endregion

    //region PRIVATE
    private String getRouterModel(GatewayDevice device) {
        return device.getModelName();
    }


    private boolean hasPreviousPort(int port) {
        if (!new File(lastPortPath).exists()) return false;
        int result = fileWriter.load(lastPortPath);
        return result == port;
    }

    private boolean isPortInRouter(int port) {
        try {
            return device.getSpecificPortMappingEntry(port, "TCP", new PortMappingEntry());
        } catch (IOException | SAXException e) {
            throw new PortForwardingException(e);
        }
    }

    private boolean forwardPort(int port, int externalPort, InetAddress localAddress) {
        try {
            boolean result = device.addPortMapping(externalPort, port, localAddress.getHostAddress(), "TCP", description);
            if (result) {
                System.out.println(port + "->" + externalPort);
                fileWriter.save(externalPort, lastPortPath);
                externalServerPort = externalPort;
            }
            return result;
        } catch (IOException | SAXException e) {
            throw new PortForwardingException(e);
        }
    }
    //endregion
}
