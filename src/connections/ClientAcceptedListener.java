package connections;

import java.net.Socket;

public interface ClientAcceptedListener<Y, T> extends IError{

    boolean onClientAccepted(Socket client); //for example - log who connected

    Y onClientObjectReceived(T receivedObject);


}
