package connections;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public interface ClientExtendedListener extends IError{
    boolean onClientAccepted(Socket client);

    OutputStream onGetOutputStream()throws FileNotFoundException;

    InputStream onSendToClientResponse()throws FileNotFoundException;

}
