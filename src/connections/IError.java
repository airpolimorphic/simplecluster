package connections;

public interface IError {
    void onExceptionOccurred(Exception e);
    void onServerClose();
}
