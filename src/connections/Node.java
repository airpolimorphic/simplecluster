package connections;

import exceptions.ClusterException;
import exceptions.IncorrectCastException;
import exceptions.ListenerNotSetException;
import exceptions.PortForwardingException;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Node {

    public static final int PORT = 9725;
    public static final int PORT_TEST = 9726;
    private String ip;
    private int serverPort;
    private int clientPort;
    private Socket client;
    private ServerSocket server;
    private boolean runAsServer;
    private ClientAcceptedListener listener;
    private ClientExtendedListener extendedListener;
    private ExecutorService executor = Executors.newCachedThreadPool();

    private ExternalPort externalPort = new ExternalPort();


    private boolean isServerEnabled = false;
    private String upnpPortDescription = "simplecluster";

    //region AS SERVER
    public Node() {
        serverPort = PORT;
        runAsServer = true;
    }

    public Node(int port) {
        serverPort = port;
        runAsServer = true;
    }
    //endregion

    //region AS CLIENT
    public Node(String ip) {
        this.ip = ip;
        clientPort = PORT;
        serverPort = PORT;
        runAsServer = false;
    }

    public Node(String ip, int port) {
        this.ip = ip;
        clientPort = port;
        serverPort = PORT;
        runAsServer = false;
    }
    //endregion

    //region CONNECTION
    public void connect() {
        try {
            if (runAsServer) {
                isServerEnabled = true;
                try {
                    serverPort = externalPort.forwardPortUpnp(serverPort, upnpPortDescription);
                } catch (PortForwardingException e) {
                    e.printStackTrace();
                }
                server = new ServerSocket(serverPort);
                server.setReuseAddress(true);
            } else {
                client = new Socket(ip, clientPort);
                client.setReuseAddress(true);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new ClusterException(e);
        }
    }

    public void disconnect() {
        try {
            if (runAsServer && server != null) {
                externalPort.removePortUpnp(externalPort.getExternalServerPort());
                isServerEnabled = false;
                executor.shutdown();
                executor.shutdownNow();
                server.close();
            }
            if (!runAsServer && client != null) client.close();
        } catch (IOException e) {
            throw new ClusterException(e);
        }
    }

    public void reconnect(boolean connect) {
        disconnect();
        if (connect) {
            connect();
        }
    }
    //endregion

    public void setUpnpPortDescription(String upnpPortDescription) {
        this.upnpPortDescription = upnpPortDescription;
    }

    public String getUpnpPortDescription() {
        return upnpPortDescription;
    }

    //region CLIENT AND SERVER
    public <T, R> R send(T data, boolean reconnect) {
        try {
            R response = null;
            sendSimple(data, client.getOutputStream());
            if (!runAsServer) {
                response = receiveSimple(client.getInputStream());
                reconnect(reconnect);
            }
            return response;
        } catch (IOException e) {
            throw new ClusterException(e);
        }
    }

    public void send(InputStream is, OutputStream responseLocation, boolean reconnect) {
        try {
            OutputStream os = client.getOutputStream();
            transit(is, os);
            is = client.getInputStream();
            if (responseLocation != null) {
                transit(is, responseLocation);
            }
            reconnect(reconnect);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ClusterException(e);
        }
    }
    //endregion

    //region SERVER SIDE ONLY
    public void setClientListener(ClientAcceptedListener listener) {
        this.listener = listener;
    }

    public void setClientExtendedListener(ClientExtendedListener extendedListener) {
        this.extendedListener = extendedListener;
    }


    public void listen() {
        if (!runAsServer) throw new ClusterException("Not a server!");
        if (listener == null && extendedListener == null) throw new ListenerNotSetException();
        executor.submit(() -> {
            while (isServerEnabled) {
                try {
                    if (server.isClosed()) break;
                    client = server.accept();
                    executor.submit(() -> {
                        try {
                            if (listener != null) {
                                boolean close = listener.onClientAccepted(client);
                                send(listener.onClientObjectReceived(receive()), !close);
                                if (close) {
                                    disconnect();
                                    listener.onServerClose();
                                    return;
                                }
                            } else if (extendedListener != null) {
                                boolean close = extendedListener.onClientAccepted(client);
                                OutputStream os = extendedListener.onGetOutputStream();
                                transit(client.getInputStream(), os);
                                InputStream is = extendedListener.onSendToClientResponse();
                                if (is != null) {
                                    os = client.getOutputStream();
                                    transit(is, os);
                                }

                                if (close) {
                                    disconnect();
                                    extendedListener.onServerClose();
                                    return;
                                }

                            }
                        } catch (Exception e) {
                            ((listener != null) ? listener : extendedListener).onExceptionOccurred(e);
                        }
                    });
                } catch (Exception e) {
                    ((listener != null) ? listener : extendedListener).onExceptionOccurred(e);
                    if (server.isClosed()) break;
                }
            }
        });


    }
    //endregion


    //region GETTERS AND SETTERS

    public String getMyExternalIp() {
        return externalPort.getMyExternalIp();
    }

    public String getIp() {
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        String out = externalPort.getMyExternalIp() != null ?
                externalPort.getMyExternalIp() :
                inetAddress.getHostAddress();

        return out;
    }

    public int getInternalServerPort() {
        return serverPort;
    }

    public int getExternalServerPort() {
        return externalPort.getExternalServerPort();
    }

    public void setLastPortPath(String lastPortPath) {
        externalPort.setLastPortPath(lastPortPath);
    }

    public String getLastPortPath() {
        return externalPort.getLastPortPath();
    }
    //endregion

    private <T> T receive() {
        try {
            return receiveSimple(client.getInputStream());
        } catch (IOException e) {
            throw new ClusterException(e);
        }
    }

    private void transit(InputStream is, OutputStream os) {
        try {
            is = new BufferedInputStream(is);
            os = new BufferedOutputStream(os);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int byteToBeRead = -1;
            while ((byteToBeRead = is.read()) != -1) {
                //need encryption;
                baos.write(byteToBeRead);
            }
            byte[] byteArray = baos.toByteArray();
            os.write(byteArray, 0, byteArray.length);
        } catch (IOException e) {
            throw new ClusterException(e);
        }
    }

    private <T> void sendSimple(T data, OutputStream os) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(os));
            oos.writeObject(data);
            oos.flush();
        } catch (IOException e) {
            throw new ClusterException(e);
        }
    }

    private <T> T receiveSimple(InputStream is) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(is));
            T out = (T) ois.readObject();
            return out;
        } catch (IOException e) {
            throw new ClusterException(e);
        } catch (ClassNotFoundException e) {
            throw new IncorrectCastException(e);
        }
    }

}
