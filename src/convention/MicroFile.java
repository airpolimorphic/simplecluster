package convention;

import exceptions.ClusterException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MicroFile implements Serializable, Iterable<Object>, Iterator<Object>{

    private String name;
    private byte[] array;
    private List<Object> params;
    private int index=-1;


    public MicroFile(String path){
        params = new ArrayList<>();
        extractByteArrayAndName(path);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getArray() {
        return array;
    }

    public void setArray(byte[] array){
        this.array = array;
    }

    public <T> void addParam(T param){
        params.add(param);
    }


    public int getParamsSize(){
        return params.size();
    }

    public <T> T next(int index){
        return (T) params.get(index);
    }


    @Override
    public boolean hasNext(){
        return params.size()>index+1;
    }

    @Override
    public Object next(){
        return params.get(++index);
    }


    public void extractByteArrayAndName(String path){
        File file = new File(path);
        name = file.getName();
        try{
            array = Files.readAllBytes(file.toPath());
        }catch (IOException e){
            throw new ClusterException(e);
        }
    }

    public void save(String path){
        try (FileOutputStream fos = new FileOutputStream(path)) {
            fos.write(array);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


    @Override
    public Iterator<Object> iterator() {
        return this;
    }
}
