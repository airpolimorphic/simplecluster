package exceptions;

public class ClusterException extends RuntimeException {

    public ClusterException(Throwable t){
        super(t);
    }

    public ClusterException(String message){
        super(message);
    }

}
