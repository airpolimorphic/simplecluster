package exceptions;

public class PortForwardingException extends RuntimeException {
    public PortForwardingException(Exception e){
        super(e);
    }
    public PortForwardingException(String message){
        super(message);
    }
}
