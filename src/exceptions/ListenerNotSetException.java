package exceptions;

public class ListenerNotSetException extends RuntimeException {
    public ListenerNotSetException(){
        super("Listener still not set! You need to set ClientAcceptedListener, and then you can use server side!");
    }
}
