package exceptions;

public class IncorrectCastException extends RuntimeException {
    public IncorrectCastException(Throwable t){
        super(t);
    }

}
